<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">name</th>
        <th scope="col">type</th>
        <th scope="col">age</th>
        <td><a class="btn btn-primary" href="{{route('animals.create')}}">Insert</a></td>

    </tr>
    </thead>
    <tbody>
    @if(isset($animals) && is_object($animals))
        @foreach($animals as $animal)
    <tr>
        <td>{{$animal->id}}</td>
        <td>{{$animal->name}}</td>
        <td>{{$animal->type}}</td>
        <td>{{$animal->age}}</td>
        <td><a class="btn btn-primary" href="{{route('animals.edit', ['id'=>$animal->id ])}}">Edit</a></td>
        <td>
            <form action="{{route('animals.destroy', ['id'=>$animal->id])}}" method="post">
                @csrf
                <input type="hidden" value="{{$animal->age}}">
                @method('DELETE')
                <button type="submit" class="btn btn-primary">Delete</button>
            </form>
        </td>
    </tr>
@endforeach
        @endif
    </tbody>
</table>
</body>
</html>