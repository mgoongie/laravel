<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MyController extends Controller
{
    public  function show(Request $request) {
     $post = $request->all();
     return view('results', compact('post'));
    }
}
